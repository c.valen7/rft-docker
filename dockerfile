FROM nginx:latest

LABEL maintainer="itzchris92@aim.com"

# Copy custom config file
COPY nginx.conf /etc/nginx/nginx.conf

